fid = 100;

//bounding values
roadway_bounds = [203.2, 38.1, 25.4];
roadway_margin = 6.35;
access = 38.1;
max_bounds = [203.2, 50.8, 127];

//values common to all pieces
tolerance = 0.1;
//this is the width of the support beams in the 1/4" side spaces
beam_y = (roadway_margin - tolerance)/2;

arch_height = 88.9;
    
//calculate the arch radius
//l = roadway_bounds[0]/2
//h = arch_height
//r = (l^2/h + h)/2
arch_radius = (pow(roadway_bounds[0]/2,2)/arch_height + arch_height)/2;
//r-h
arch_zdisp = arch_radius - arch_height;
    
support_length = 55;

//draw the maximum bounds for reference
/*
%difference() {
    translate([-max_bounds[0]/2, -max_bounds[1]/2, 0]) cube(max_bounds);
    
    translate([-roadway_bounds[0]/2-1, -roadway_bounds[1]/2, roadway_margin]) 
        cube(roadway_bounds+[2,0,0]);
    
    translate([-access/2, -access/2, roadway_margin]) cube([access, access, max_bounds[2]]);
}*/

//Place the assembly

translate([0,roadway_margin-max_bounds[1]/2-beam_y]) rotate([0,0,180]) arch();

translate([0,max_bounds[1]/2-roadway_margin+beam_y]) arch();

road();


//support();

module arch() {
    arch_thickness = 21;
    
    translate([0,0,-arch_zdisp])
    rotate([90,0,0])//rotate the arch upright
    difference() {
        cylinder(beam_y, r=arch_radius, $fn=fid);
        
        //round the edge
        rotate_extrude($fn=fid) translate([arch_radius-beam_y,0,0]) archRoundingProfile();
        
        //remove the inside
        translate([0,4,-1]) cylinder(beam_y+2, r=arch_radius-arch_thickness+4, $fn=fid);
        
        //remove the chunk that will stick out of the bounds
        translate([-200,arch_zdisp-200,-200]) cube([400, 200, 400]);
    }
    //add cross braces?
    for(k=[60,45,30,-30,-45,-60]) {
      translate([sin(k)*(arch_radius-11),0,cos(k)*(arch_radius-11)-11]) rotate([90,0,0]) cylinder(r=4,h=40,$fn=10);
    }
    
    //add the horizontal linking member
    difference() {
        translate([-arch_radius+arch_thickness-4, -beam_y, 0])
            cube([(arch_radius-arch_thickness)*2+8, beam_y, arch_thickness/2]);
        
        //subtract the path for the support
        translate([support_length/2,-beam_y-1,0]) rotate([0,-90,0])
            linear_extrude(support_length) difference() {
                square([beam_y*2+tolerance, beam_y+2]);
                
                translate([beam_y*2+tolerance,1,0]) circle(beam_y, $fn=fid/2);
            }
            
        copyMirror([1,0,0]) {
            //cut out a 45 degree fillet
            translate([support_length/2,-beam_y,0]) 
                rotate([0,0,-90])
                rotate([0,-90,0])
                rotate_extrude(angle=45, $fn=fid/2) difference() {
                    square([beam_y*2+tolerance, beam_y+1]);
                    
                    translate([beam_y*2+tolerance,0,0]) circle(beam_y, $fn=fid/2);
                }
                
            //cut out from the 45 degree point all the way through the bottom
            translate([support_length/2, -1-beam_y,0]) rotate([0,45,0]) {
                difference() {
                    cube([20, beam_y+2, beam_y*2+tolerance]);
                    
                    translate([0,1,beam_y*2+tolerance]) rotate([0,90,0]) 
                        cylinder(20, r=beam_y, $fn=fid/2);
                }
            }
        }
    }
}

module archRoundingProfile() {
    difference() {
        square([beam_y+1,beam_y+1]);
        
        circle(beam_y, $fn=fid/2);
    }
}

module support() translate([-support_length/2,0,0]) {
    height_above_arch = 12.7;
    support_height = arch_height + height_above_arch;
    
    cross_height = 5;
    cross_length = 7;
    cross_fillet = 3;
    
    intersection() {
        difference() {
            //the primary volume
            hull() {
                translate([0,max_bounds[1]/2-beam_y,beam_y]) rotate([0,90,0]) 
                    cylinder(support_length, r=beam_y, $fn=fid);
                translate([0,-max_bounds[1]/2+beam_y,beam_y]) rotate([0,90,0]) 
                    cylinder(support_length, r=beam_y, $fn=fid);
                
                translate([0,-max_bounds[1]/2,support_height])
                    cube([support_length, max_bounds[1], 1]);
            }
            //cut out the middle
            hull() {
                translate([-1,max_bounds[1]/2-beam_y*2,beam_y*2]) rotate([0,90,0]) 
                    cylinder(support_length+2, r=beam_y, $fn=fid);
                translate([-1,-max_bounds[1]/2+beam_y*2,beam_y*2]) rotate([0,90,0]) 
                    cylinder(support_length+2, r=beam_y, $fn=fid);
                
                translate([-1,-max_bounds[1]/2+beam_y,support_height])
                    cube([support_length+2, max_bounds[1]-beam_y*2, 2]);
            }
            
            //cut out a diamond from the sides
            hull() {
                translate([support_length/2,-100,20]) rotate([-90,0,0]) cylinder(200, r=10, $fn=fid);
                translate([support_length/2-5,-100,45]) rotate([-90,0,0]) cylinder(200, r=10, $fn=fid);
                translate([support_length/2+5,-100,45]) rotate([-90,0,0]) cylinder(200, r=10, $fn=fid);
                translate([support_length/2,-100,70]) rotate([-90,0,0]) cylinder(200, r=10, $fn=fid);
            }
        }
        
        //cut out the corners above the rounded top
        translate([support_length/2,-1-max_bounds[1]/2,-arch_zdisp]) rotate([-90,0,0])
            cylinder(max_bounds[1]+2, r=(arch_radius+height_above_arch), $fn=fid);
    }
    
    //add hooks
    copyMirror([0,1,0])
    translate([support_length/2,max_bounds[1]/2-beam_y,-arch_zdisp]) 
    rotate([90,0,0]) difference() {
        cylinder(beam_y+tolerance, r=arch_radius+height_above_arch, $fn=fid);
        
        rotate_extrude($fn=fid) translate([arch_radius+tolerance-beam_y,0,0]) 
            circle(beam_y+tolerance, $fn=fid/2);
        
        translate([0,0,-1]) 
            cylinder(beam_y+tolerance+2, r=(arch_radius+tolerance-beam_y), $fn=fid);
        
        //cutoff z<=0
        translate([-200, -200, -200]) cube([400, 200, 400]);
        
        //cutoff sides
        translate([-support_length/2-200,-200,-200]) cube([200, 400, 400]);
        translate([support_length/2,-200,-200]) cube([200, 400, 400]);
    }
    
    //add the cross-piece to hold the sides steady
    translate([0,0,arch_height-2]) difference() {
        translate([0,-roadway_bounds[1]/2-0.05,-cross_fillet])
            cube([cross_length, roadway_bounds[1]+0.1, cross_height+cross_fillet*2]);
        
        //fillets
        translate([-1, 0, -cross_fillet]) {
            hull() {
                translate([0,cross_fillet-roadway_bounds[1]/2,0])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
                translate([0,-cross_fillet+roadway_bounds[1]/2,0])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
            }
            
            hull() {
                translate([0,cross_fillet-roadway_bounds[1]/2,cross_height+cross_fillet*2])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
                translate([0,-cross_fillet+roadway_bounds[1]/2,cross_height+cross_fillet*2])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
            }
        }
    }
    
    
        translate([support_length-cross_length,0,arch_height-2]) difference() {
        translate([0,-roadway_bounds[1]/2-0.05,-cross_fillet])
            cube([cross_length, roadway_bounds[1]+0.1, cross_height+cross_fillet*2]);
        
        //fillets
        translate([-1, 0, -cross_fillet]) {
            hull() {
                translate([0,cross_fillet-roadway_bounds[1]/2,0])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
                translate([0,-cross_fillet+roadway_bounds[1]/2,0])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
            }
            
            hull() {
                translate([0,cross_fillet-roadway_bounds[1]/2,cross_height+cross_fillet*2])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
                translate([0,-cross_fillet+roadway_bounds[1]/2,cross_height+cross_fillet*2])
                    rotate([0,90,0]) cylinder(cross_length+2, r=cross_fillet, $fn=fid);
            }
        }
    }
    
}

module road() {
    ramp_length = 25.4;
    
    //Add an extra section at the end to cover up the arch fillet
    copyMirror([1,0,0]) 
        translate([-roadway_bounds[0]/2,-3-roadway_bounds[1]/2,0]) {
            
        cube([5, roadway_bounds[1]+6, roadway_margin-2.5]);
    }
    
    difference() {
    //the main rode surface
      union() {
        translate([-roadway_bounds[0]/2, -(roadway_bounds[1]+tolerance)/2,0])
            cube([roadway_bounds[0], roadway_bounds[1]+1, roadway_margin-2.5]);
      }
      
      //cut out the middle for the support
      translate([-support_length/2-1, -50, -50]) cube([support_length+2, 100, 100]);
      
     translate([-roadway_bounds[0]/2+20, -(roadway_bounds[1]+tolerance)/2,-0.5])
         cube([roadway_bounds[0]-40, roadway_bounds[1]+1, roadway_margin-2.5]);
      support();
    }
          
    copyMirror([1,0,0]) difference() {
        translate([support_length/2+4.5, -roadway_bounds[1]/2, 0])
        rotate([0,-45,0]) cube([3, roadway_bounds[1], 10]);
        
        translate([-100, -50, roadway_margin-3]) cube([200, 100, 100]);
    }

}


module copyMirror(vec){
    children();
    mirror(vec) children();
}